import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.regex.Pattern;

public class Client {
	private static Socket socket;

	// Application client
	public static void main(String[] args) throws Exception {
		/*	Scanner addressIP = new Scanner(System.in);
		System.out.println("Veuillez entrer une adresse IP ");
		 */	

		// Adresse et port du serveur

		String serverAddress = "132.207.29.108";
		int serverPort = 5000;

		// Cr�ation d' une connexion avec le serveur

		socket = new Socket(serverAddress, serverPort);
		System.out.format("The server from client is running %s:%d%n", serverAddress, serverPort);

		// Cr�ation d'un canal pour recevoir les messages du serveur

		DataInputStream in = new DataInputStream (socket.getInputStream());
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());

		// R�ception du message et impression

		String helloMessageFromServer = in.readUTF();
		System.out.println(helloMessageFromServer);
		out.writeUTF("Message re�u");
		// Fermeture de la connexion avec le serveur
		socket.close();

	}

	//Taken from https://stackoverflow.com/questions/5667371/validate-ipv4-address-in-java
	private static final Pattern PATTERN = Pattern.compile(
			"^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

	public static boolean validateIpAdress(final String ipAdress) {
		return PATTERN.matcher(ipAdress).matches();
	}
}




