import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
	private static ServerSocket listener;

	// Aplication Serveur

	public static void main(String[] args) throws Exception {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Veuillez entrer une adresse IP ");
		String ipAddress = keyboard.nextLine();
		System.out.println("resultat " + Client.validateIpAdress(ipAddress));;

		
		// Compteur de connexion
		int clientNumber = 0;

		// Adresse et port du serveur

		String serverAddress = "132.207.29.108";
		int serverPort = 5000;

		// Cr�ation d'une connexion avec les clients

		listener = new ServerSocket();
		listener.setReuseAddress(true);
		InetAddress serverIP = InetAddress.getByName(serverAddress);

		// Association de l'adresse et du port

		listener.bind(new InetSocketAddress(serverIP, serverPort));
		System.out.format("The server is running from server %s:%d%n", serverAddress, serverPort);

		
		try 
		{
			// Pour chaque connexion d'un client
			while (true) {
				// On attend le prochain client
				// Note: la fonction accept est bloquante

				new ClientHandler(listener.accept(), clientNumber++).start();
			}
		}

		finally 
		
		{
			// Fermeture de la connexion avec le client
			listener.close();
		}
	}

	private static class ClientHandler extends Thread {
		private Socket socket;
		private int clientNumber;

		public ClientHandler(Socket socket, int clientNumber) {
			this.socket = socket;
			this.clientNumber = clientNumber;

			System.out.println("New connection with client #" + clientNumber + " at " + socket);

		}

		public void run() {
			try {
				// Cr�ation d'un canal pour envoyer des messages au client

				DataOutputStream out = new DataOutputStream(socket.getOutputStream());

				// Envoie d'un message
				// write acts like a semaphore else deadlock
				out.writeUTF("Hello from server - you are client#" + clientNumber);
				
				DataInputStream in = new DataInputStream (socket.getInputStream());
				String helloMessageFromServer = in.readUTF();
				System.out.println(helloMessageFromServer);


			} catch (IOException e) {
				System.out.println("Error handling client# " + clientNumber + " : " + e);
			} finally {
				try {
					// Fermeture de la connexion avec le client

					socket.close();

				} catch (IOException e) {
					System.out.println("Could not close a socket");
				}
				System.out.println("Connection with client#" + clientNumber + " closed");
			}
		}

	}

}
